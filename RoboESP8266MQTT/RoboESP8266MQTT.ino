#include <ESP8266WiFi.h> // Importa a Biblioteca ESP8266WiFi
#include <PubSubClient.h> // Importa a Biblioteca PubSubClient

//defines: id mqtt e tópicos para publicação e subscribe

#define TOPICO_SUBSCRIBE "robo/1/sub" //tópico MQTT de escuta
#define TOPICO_PUBLISH   "robo/1/pub" //tópico MQTT de envio de informações para Broker
                                      //IMPORTANTE: recomendamos fortemente alterar os nomes
                                      //            desses tópicos. Caso contrário, há grandes
                                      //            chances de você controlar e monitorar o NodeMCU
                                      //            de outra pessoa.
                                       
#define ID_MQTT  "CARRO_01" //id mqtt (para identificação de sessão)
                            //IMPORTANTE: este deve ser único no broker (ou seja, 
                            //            se um client MQTT tentar entrar com o mesmo 
                            //            id de outro já conectado ao broker, o broker 
                            //            irá fechar a conexão de um deles).
                               

//defines - mapeamento de pinos do NodeMCU
#define D0    16
#define D1    5
#define D2    4
#define D3    0
#define D4    2
#define D5    14
#define D6    12
#define D7    13
#define D8    15
#define D9    3
#define D10   1

// WIFI
const char* SSID = "wrobos"; // SSID / nome da rede WI-FI que deseja se conectar
const char* PASSWORD = "46503510"; // Senha da rede WI-FI que deseja se conectar
 
// MQTT
const char* BROKER_MQTT = "roboserver.roboslan.betim.ifmg.com.br"; //URL do broker MQTT que se deseja utilizar
int BROKER_PORT = 1883; // Porta do Broker MQTT

//Variáveis e objetos globais
WiFiClient espClient; // Cria o objeto espClient
PubSubClient MQTT(espClient); // Instancia o Cliente MQTT passando o objeto espClient
 
//Prototypes
void initSerial();
void initWiFi();
void initMQTT();
void reconectWiFi(); 
void mqtt_callback(char* topic, byte* payload, unsigned int length);
void VerificaConexoesWiFIEMQTT(void);
void InitOutput(void);

/* 
 *  Implementações das funções
 */
void setup() 
{
    ESP.eraseConfig();
    WiFi.mode(WIFI_STA);
    
    //inicializações:
    InitOutput();
    initSerial();
    initWiFi();
    initMQTT();
}
 
//Função: inicializa comunicação serial com baudrate 115200 (para fins de monitorar no terminal serial 
//        o que está acontecendo.
//Parâmetros: nenhum
//Retorno: nenhum
void initSerial() 
{
    Serial.begin(115200);
}

//Função: inicializa e conecta-se na rede WI-FI desejada
//Parâmetros: nenhum
//Retorno: nenhum
void initWiFi() 
{
    delay(10);
    Serial.println("------Conexao WI-FI------");
    Serial.print("Conectando-se na rede: ");
    Serial.println(SSID);
    Serial.println("Aguarde");
    
    reconectWiFi();
}
 
//Função: inicializa parâmetros de conexão MQTT(endereço do 
//        broker, porta e seta função de callback)
//Parâmetros: nenhum
//Retorno: nenhum
void initMQTT() 
{
    MQTT.setServer(BROKER_MQTT, BROKER_PORT);   //informa qual broker e porta deve ser conectado
    MQTT.setCallback(mqtt_callback);            //atribui função de callback (função chamada quando qualquer informação de um dos tópicos subescritos chega)
}

String toString(byte* payload, unsigned int length) {
    
    String msg;
    
    //obtem a string do payload recebido
    for(int i = 0; i < length; i++) 
    {
       char c = (char)payload[i];
       msg += c;
    }

    return msg;
}


void command(String lE, int vE, String lF, int vF, String lD, int vD) {

    if (vE > 0) {
      analogWrite(D1, map(vE, 1, 99, 966, 1023));
    } else {
      analogWrite(D1, 0);
    }

    if (vD > 0) {
      analogWrite(D2, map(vD, 1, 99, 966, 1023));
    } else {
      analogWrite(D2, 0);
    }
    
    if (lE.equals("F")) {
      digitalWrite(D3, 0);
    } else {
      digitalWrite(D3, 1);
    }

    if (lD.equals("F")) {
      digitalWrite(D4, 0);
    } else {
      digitalWrite(D4, 1);
    }
    
    /*
    if (lE.equals("F")) {
      digitalWrite(D1, 1);
      analogWrite(D3, vE);
    } else {
      
    }
    
    if (lD.equals("F")) {
      digitalWrite(D2, 1);
      analogWrite(D4, vD);
    } else {
      
    }
    */
}

//Função: função de callback 
//        esta função é chamada toda vez que uma informação de 
//        um dos tópicos subescritos chega)
//Parâmetros: nenhum
//Retorno: nenhum
void mqtt_callback(char* topic, byte* payload, unsigned int length) 
{
    
    String message = "";
    
    if (length == 9) {

      int i = 0;
      String v1 = "";
      String v2 = "";
      String v3 = "";
      
      String letraEsquerdo = "";
      letraEsquerdo += (char)payload[i++];

      v1 = ((char)payload[i++]);
      v2 = ((char)payload[i++]);
      //v3 = ((char)payload[i++]);
      
      int    valorEsquerdo = (v1.toInt() * 10) + (v2.toInt() * 1);

      String letraFrente = "";
      letraFrente += (char)payload[i++];

      v1 = ((char)payload[i++]);
      v2 = ((char)payload[i++]);
      //v3 = ((char)payload[i++]);
      
      int    valorFrente = (v1.toInt() * 10) + (v2.toInt() * 1);

      String letraDireito = "";
      letraDireito += (char)payload[i++];

      v1 = ((char)payload[i++]);
      v2 = ((char)payload[i++]);
      //v3 = ((char)payload[i++]);
      
      int    valorDireito = (v1.toInt() * 10) + (v2.toInt() * 1);

      command(letraEsquerdo, valorEsquerdo, letraFrente, valorFrente, letraDireito, valorDireito);
      
      message += "COMMAND => " + letraEsquerdo + ": " + valorEsquerdo + " | " + letraFrente   + ": " + valorFrente + " | " + letraDireito  + ": " + valorDireito;

      delay(500);
      
      command(letraEsquerdo, 0, letraFrente, 0, letraDireito, 0);
      
    } else {
      
      message = "LENGTH:" + length;
    }

    const char * msg = message.c_str();
    
    EnviaEstadoOutputMQTT(msg);
}
 
//Função: reconecta-se ao broker MQTT (caso ainda não esteja conectado ou em caso de a conexão cair)
//        em caso de sucesso na conexão ou reconexão, o subscribe dos tópicos é refeito.
//Parâmetros: nenhum
//Retorno: nenhum
void reconnectMQTT() 
{
    while (!MQTT.connected()) 
    {
        Serial.print("* Tentando se conectar ao Broker MQTT: ");
        Serial.println(BROKER_MQTT);
        if (MQTT.connect(ID_MQTT, "mosquitto", "mosquitto")) 
        {
            Serial.println("Conectado com sucesso ao broker MQTT!");
            MQTT.subscribe(TOPICO_SUBSCRIBE); 
        } 
        else 
        {
            Serial.println("Falha ao reconectar no broker.");
            Serial.println("Havera nova tentatica de conexao em 2s");
            delay(2000);
        }
    }
}
 
//Função: reconecta-se ao WiFi
//Parâmetros: nenhum
//Retorno: nenhum
void reconectWiFi() 
{
    // Se já está conectado a rede WI-FI, nada é feito. 
    // Caso contrário, são efetuadas tentativas de conexão
    if (WiFi.status() == WL_CONNECTED) {
        return;
    }
    
    WiFi.hostname(ID_MQTT);
    WiFi.begin(SSID, PASSWORD); // Conecta na rede WI-FI
    
    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(100);
        Serial.print(".");
    }
  
    Serial.println();
    Serial.print("Conectado com sucesso na rede.");
    Serial.print(SSID);
    Serial.println("IP obtido: ");
    Serial.println(WiFi.localIP());
}

//Função: verifica o estado das conexões WiFI e ao broker MQTT. 
//        Em caso de desconexão (qualquer uma das duas), a conexão
//        é refeita.
//Parâmetros: nenhum
//Retorno: nenhum
void VerificaConexoesWiFIEMQTT(void)
{
    if (!MQTT.connected()) 
        reconnectMQTT(); //se não há conexão com o Broker, a conexão é refeita
    
     reconectWiFi(); //se não há conexão com o WiFI, a conexão é refeita
}

//Função: envia ao Broker o estado atual do output 
//Parâmetros: nenhum
//Retorno: nenhum
void EnviaEstadoOutputMQTT(const char * out)
{
    MQTT.publish(TOPICO_PUBLISH, out);
}

//Função: inicializa o output em nível lógico baixo
//Parâmetros: nenhum
//Retorno: nenhum
void InitOutput(void)
{
    //IMPORTANTE: o Led já contido na placa é acionado com lógica invertida (ou seja,
    //enviar HIGH para o output faz o Led apagar / enviar LOW faz o Led acender)
    /*
    pinMode(D0, OUTPUT);
    digitalWrite(D0, HIGH);
    */

    pinMode(D1, OUTPUT);
    pinMode(D2, OUTPUT);
    pinMode(D3, OUTPUT);
    pinMode(D4, OUTPUT);

    digitalWrite(D1, 0);
    digitalWrite(D2, 0);
    
    digitalWrite(D3, 0);
    digitalWrite(D4, 0);
}


//programa principal
void loop() 
{   
    //garante funcionamento das conexões WiFi e ao broker MQTT
    VerificaConexoesWiFIEMQTT();

    //keep-alive da comunicação com broker MQTT
    MQTT.loop();
}
