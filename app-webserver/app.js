const express = require('express')
const app = express()

var mqtt = require('mqtt');

app.get('/', function(req, res) {
	res.send('Service listed!');
});

app.get('/robo/:robo/command/:command', function(req, res) {

	//console.log(req.params);

	let robo = 'robo/' + req.params['robo'];
	let command = req.params['command'];

	var client = mqtt.connect(
		'mqtt://roboserver.roboslan.betim.ifmg.com.br', {
			username: 'mosquitto',
			password: 'mosquitto'
		}
	);

	client.on('connect', function() {
		client.subscribe(robo + '/pub');
		client.publish(robo + '/sub', command);
	});

	client.on('error', function(){
		console.log("ERROR");
		client.end();
	});

	client.on('message', function(topic, message) {

		let result = 'ROBO: '      + req.params['robo'] + 
					 ' / COMMAND: ' + req.params['command']
					 ' / MESSAGE: ' + message.toString();

		console.log(result);
			
		client.end();
	});

	res.send(command);
});

const PORT = 8888;

app.listen(PORT, function() {
	console.log('App listening on port ' + PORT + '!');
});