var mqtt = require('mqtt');

var client = mqtt.connect('mqtt://roboserver.roboslan.betim.ifmg.com.br', {
	username: 'mosquitto',
	password: 'mosquitto'
});

client.on('connect', function() {
	client.subscribe('robo/1/sub');
});

client.on('message', function(topic, message) {
	console.log('robo/1/pub', message.toString());
	client.publish('robo/1/pub', message.toString());
});