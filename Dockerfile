FROM ubuntu:xenial
MAINTAINER Mauricio Monteiro <mauricio.monteiro@gmail.com>

ENV OS_LOCALE="pt_BR.UTF-8"
RUN locale-gen ${OS_LOCALE}
ENV LANG=${OS_LOCALE} \
    LANGUAGE=pt_BR:pt \
    LC_ALL=${OS_LOCALE} \
    DEBIAN_FRONTEND=noninteractive

RUN \
    buildDeps='software-properties-common python-software-properties' \
    && apt-get update \
    # Install common libraries
    && apt-get install --no-install-recommends -y $buildDeps \
    && add-apt-repository -y ppa:mosquitto-dev/mosquitto-ppa \
    && apt-get update \
    && apt-get upgrade -y \
    # Install supervisor
    && apt-get install -y supervisor && mkdir -p /var/log/supervisor \
    # Install Mosquitto
    && apt-get install -y mosquitto mosquitto-clients \
    # Cleaning
    && apt-get purge -y --auto-remove $buildDeps \
    && apt-get autoremove -y && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY ./docker-entrypoint.sh /docker-entrypoint.sh

RUN mkdir -p /mosquitto/log && mkdir -p /mosquitto/config && mkdir -p /mosquitto/data

RUN touch /mosquitto/log/mosquitto.log

RUN chmod -R 777 /mosquitto/log

COPY ./mosquitto.conf /mosquitto/config/mosquitto.conf

RUN touch /mosquitto/config/pwfile \
    && mosquitto_passwd -b /mosquitto/config/pwfile mosquitto mosquitto
	
EXPOSE 1883 9001

ENTRYPOINT ["/docker-entrypoint.sh"]
	
CMD ["/usr/sbin/mosquitto", "-c", "/mosquitto/config/mosquitto.conf"]